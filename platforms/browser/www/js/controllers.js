angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $http,$ionicModal, $rootScope,$timeout,$localStorage,$ionicSideMenuDelegate,$ionicActionSheet,$state) {

	$scope.isEnter = 0;
	$scope.sideMenuWidth = window.innerWidth*0.9;
	$scope.isLocalStorage = 0;
	

	$scope.$watch(function () 
	{
		return $ionicSideMenuDelegate.isOpenLeft();
	  },
	  function (isOpen) 
      {
		if (!isOpen && $scope.isEnter == 0)
		{
			$scope.isEnter = 1;
			setTimeout(function()
			{ 
				$rootScope.$broadcast('Matches_Controller');
				$rootScope.$broadcast('Likes_Controller');
			}, 100);	
		}
	});



})


.controller('MainController', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,OpenFB,$ionicPopup,$ionicSideMenuDelegate,$q, UserService, $ionicLoading) 
{

	$rootScope.currentPage = "main"
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>';
//	$ionicSideMenuDelegate.canDragContent(false);
	
	//alert ($localStorage.userid);
	
	
	if ($localStorage.userid)
	{
		$scope.isLocalStorage = 1;
		$ionicSideMenuDelegate.canDragContent(true);
		$state.go('app.matches');
		//alert("True")
	}
	else
	{
		$ionicSideMenuDelegate.canDragContent(false);
		//alert("False")
	}


  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {	
	$scope.facebookImage = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
	$scope.FacebookIdLogin(profileInfo.id,profileInfo.name,profileInfo.email,profileInfo.gender,$scope.facebookImage);
	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,gender&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookConnect = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
	//alert (success.status);
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);
			
					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {
						// For the purpose of this example I will store user data on local storage
						$scope.facebookImage = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
						$scope.FacebookIdLogin(profileInfo.id,profileInfo.name,profileInfo.email,profileInfo.gender,$scope.facebookImage);
						
						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
	
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

		console.log('getLoginStatus', success.status);

		$ionicLoading.show({
          template: 'Loading...'
        });

		// Ask the permissions you need. You can learn more about
		// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
	        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };

  
	$scope.FacebookIdLogin = function(FBId,name,email,gender,photo)
	{

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			login_params = {
			"facebookid" : FBId,
			"name" : name,
			"email" : email,
			"gender" : gender,
			"photo" : photo
			}
		//console.log(login_params)
		$http.post($rootScope.host+'/login.php',login_params)
		.success(function(data, status, headers, config)
		{
		
		/*
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				title: 'error signing in using facebook please try again',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
				});				   
			}
			else 
			{		
		*/
				$localStorage.userid = data.response.userid;
				$localStorage.username = data.response.username;
				$localStorage.email = data.response.email;
				$localStorage.gender = data.response.gender;
				$localStorage.interested = data.response.interested;
				$localStorage.visiblity = data.response.visiblity;				
				$localStorage.age = data.response.age;
				$localStorage.minAge = data.response.minAge;
				$localStorage.MaxAge = data.response.maxAge;	
				//alert (data.response.minAge)
				//alert (data.response.maxAge)
				$localStorage.nearby = data.response.nearby
				$localStorage.distance = data.response.distance			
				$localStorage.image = data.response.image;
				
				setTimeout(function()
				{ 
					$rootScope.$broadcast('Matches_Controller');
					$rootScope.$broadcast('Likes_Controller');
				}, 100);
					
				$state.go('app.matches');
				$ionicSideMenuDelegate.canDragContent(true);
				//console.log($localStorage)
		//	}
		})
		.error(function(data, status, headers, config)
		{

		});

				
	}
  

})


.controller('MenuController', function($scope,$http,$rootScope,$stateParams) 
{
	$ionicSideMenuDelegate.canDragContent(true);
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>';
	
	//$scope.$on('$ionicView.enter', function(e) 
    //{
	
	$scope.navigateUrl = function (Path,Num) 
	{
		setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
	}


})



.controller('LoginCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicPopup,$ionicSideMenuDelegate,$q, UserService, $ionicLoading,$ionicModal) 
{
	$rootScope.currentPage = "login"
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>'
	$scope.loginFields = {
		"username" : "",
		"password": ""
	};
	
	$scope.forgot = 
	{
		"email" : ""
	}
	
	$scope.doLogin = function()
	{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			login_params = {
					"username" : $scope.loginFields.username,
					"password" : $scope.loginFields.password
					}
				//console.log(login_params)
				$http.post($rootScope.host+'/login.php',login_params)
				.success(function(data, status, headers, config)
				{
					if (data.response.status == 0)
					{
						 $ionicPopup.alert({
						 title: 'Email or Password don\'t match please try again',
						  buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });				   
					}
					else 
					{		
					$localStorage.userid = data.response.userid;
					$localStorage.username = data.response.username;
					$localStorage.email = data.response.email;
					$localStorage.gender = data.response.gender;
					$localStorage.interested = data.response.interested;
					$localStorage.visiblity = data.response.visiblity;				
					$localStorage.age = data.response.age;
					$localStorage.minAge = data.response.minAge;
					$localStorage.MaxAge = data.response.maxAge;	
					$localStorage.nearby = data.response.nearby
					$localStorage.distance = data.response.distance
					$localStorage.image = data.response.image;
					
					setTimeout(function()
					{ 
						$rootScope.$broadcast('Matches_Controller');
						$rootScope.$broadcast('Likes_Controller');
					}, 100);
						
					$state.go('app.matches');
					//console.log($localStorage)
					}
				})
				.error(function(data, status, headers, config)
				{

				});
	}
	

  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {	
	$scope.facebookImage = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
	$scope.FacebookIdLogin(profileInfo.id,profileInfo.name,profileInfo.email,profileInfo.gender,$scope.facebookImage);
	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,gender&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookConnect = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
	//alert (success.status);
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);
			
					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {
						// For the purpose of this example I will store user data on local storage
						$scope.facebookImage = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
						$scope.FacebookIdLogin(profileInfo.id,profileInfo.name,profileInfo.email,profileInfo.gender,$scope.facebookImage);
						
						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
	
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

		console.log('getLoginStatus', success.status);

		$ionicLoading.show({
          template: 'Loading...'
        });

		// Ask the permissions you need. You can learn more about
		// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
	        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };

  
	$scope.FacebookIdLogin = function(FBId,name,email,gender,photo)
	{

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			login_params = {
			"facebookid" : FBId,
			"name" : name,
			"email" : email,
			"gender" : gender,
			"photo" : photo
			}
		//console.log(login_params)
		$http.post($rootScope.host+'/login.php',login_params)
		.success(function(data, status, headers, config)
		{
		
		/*
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				title: 'error signing in using facebook please try again',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
				});				   
			}
			else 
			{		
		*/
				$localStorage.userid = data.response.userid;
				$localStorage.username = data.response.username;
				$localStorage.email = data.response.email;
				$localStorage.gender = data.response.gender;
				$localStorage.interested = data.response.interested;
				$localStorage.visiblity = data.response.visiblity;				
				$localStorage.age = data.response.age;
				$localStorage.minAge = data.response.minAge;
				$localStorage.MaxAge = data.response.maxAge;	
				//alert (data.response.minAge)
				//alert (data.response.maxAge)
				$localStorage.nearby = data.response.nearby
				$localStorage.distance = data.response.distance				
				$localStorage.image = data.response.image;
				
				setTimeout(function()
				{ 
					$rootScope.$broadcast('Matches_Controller');
					$rootScope.$broadcast('Likes_Controller');
				}, 100);
					
				$state.go('app.matches');
				$ionicSideMenuDelegate.canDragContent(true);
				//console.log($localStorage)
		//	}
		})
		.error(function(data, status, headers, config)
		{

		});

				
	}

	$scope.forgotPass = function()
	{
			$ionicModal.fromTemplateUrl('templates/forgot_password.html', {
			scope: $scope
		  }).then(function(forgotPassModal) {
			$scope.forgotPassModal = forgotPassModal;
			$scope.forgotPassModal.show();
		  });
	}
	
	$scope.closeForgotPass = function()
	{
		$scope.forgotPassModal.hide();
	}
	
	$scope.sendPass = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.forgot.email =="")
		{
			$ionicPopup.alert({
			title: 'please input your email address',
			buttons: [{
			text: 'OK',
			type: 'button-positive',
			  }]
		   });	

		 }
		else if (emailRegex.test($scope.forgot.email) == false)
		{
			$ionicPopup.alert({
			title: 'email address not valid please correct',
			buttons: [{
			text: 'OK',
			type: 'button-positive',
			  }]
		   });	

		   $scope.forgot.email =  '';
		}
		else
		{

		send_params = 
		{
			"email" : $scope.forgot.email
		}
		//console.log(login_params)
		$http.post($rootScope.host+'/forgot_pass.php',send_params)
		.success(function(data, status, headers, config)
		{
		})
		.error(function(data, status, headers, config)
		{

		});
		
			
			$ionicPopup.alert({
			title: 'your password has been sent to your email',
			buttons: [{
			text: 'OK',
			type: 'button-positive',
			  }]
		   });				
			
			$scope.forgot.email =  '';
			$scope.forgotPassModal.hide();
			
			
		}
		
		
	}
	
	
})


.controller('RegisterCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$ionicPlatform,$state,$ionicActionSheet,$cordovaCamera,$timeout,$ionicPopup,$ionicSideMenuDelegate,$ionicModal,OpenFB,$q, UserService, $ionicLoading) 
{
	
	 //var isVisible = $cordovaKeyboard.isVisible();
	 //alert (isVisible);

	$scope.keyboardOpen = 0;
	$rootScope.currentPage = "register"
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>';
	$scope.profilePicture = 'img/main/addphoto.png';
	$scope.registerFields = 
	{
		"name" : "",
		"username" : "",
		"email" : "",
		"password" : "",
		"gender" : "1",
		"interested" : "2",
		"age" : "1",
		"minAge" : "18",
		"maxAge" : "55",
		"facebookid" : ""
	};
	
	$scope.image = 
	{	
		"defaultimage":'img/register/camera.png',
		"submit":false
	};

	window.addEventListener('native.keyboardhide', keyboardHideHandler);

	function keyboardHideHandler(e){
	   $timeout(function() {
		 $scope.doBlur();
	   }, 300);
	}	
	
	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		//alert("BackButton")
	   $timeout(function() {
		 $scope.doBlur();
	   }, 300);
   
	 	
  	}, 300);

	$scope.doFocus = function()
	{
		$scope.keyboardOpen = 1;
	}
	
	$scope.doBlur = function()
	{
		$scope.keyboardOpen = 0;
	}
	
	
	
	$scope.registerFields.age = 18;

	var agesArray = Array();

   for(var i = 18;i <100 ;i++)
    {
		 agesArray.push(i);
	}		
	
	$scope.ages = agesArray;


	$scope.TakeFaceBookPhoto = function()
	{
		$scope.faceBookLogin(1);
	}
	
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse,type)
    .then(function(profileInfo) {	
	$scope.facebookImage = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
	
	if (type == 0)
	{
		$scope.FacebookIdLogin(profileInfo.id,profileInfo.name,profileInfo.email,profileInfo.gender,$scope.facebookImage);
	}
	else
	{
	 // $scope.registerFields.name = profileInfo.name;
	 // $scope.registerFields.email = profileInfo.email;
	  $scope.profilePicture = 'https://graph.facebook.com/'+profileInfo.id+'/picture?type=large';
	  $scope.responseimage = $scope.profilePicture;
	  $scope.image.defaultimage = $scope.profilePicture;		    
	  $scope.registerFields.facebookid = profileInfo.id;		
	}
	
	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,gender&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.faceBookLogin = function(type) {
    facebookConnectPlugin.getLoginStatus(function(success){
	//alert (success.status);
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);
			
					getFacebookProfileInfo(success.authResponse,type)
					.then(function(profileInfo) {
						// For the purpose of this example I will store user data on local storage
						$scope.facebookImage = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
						
						
						if (type == 0)
						{
							$scope.FacebookIdLogin(profileInfo.id,profileInfo.name,profileInfo.email,profileInfo.gender,$scope.facebookImage);
						}
						else
						{
						 // $scope.registerFields.name = profileInfo.name;
						 // $scope.registerFields.email = profileInfo.email;
						  $scope.profilePicture = 'https://graph.facebook.com/'+profileInfo.id+'/picture?type=large';
						  $scope.responseimage = $scope.profilePicture;
						  $scope.image.defaultimage = $scope.profilePicture;		    
						  $scope.registerFields.facebookid = profileInfo.id;		
						}
						
						
						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
	
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

		console.log('getLoginStatus', success.status);

		$ionicLoading.show({
          template: 'Loading...'
        });

		// Ask the permissions you need. You can learn more about
		// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
	        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };

  
	$scope.FacebookIdLogin = function(FBId,name,email,gender,photo)
	{

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			login_params = {
			"facebookid" : FBId,
			"name" : name,
			"email" : email,
			"gender" : gender,
			"photo" : photo
			}
		//console.log(login_params)
		$http.post($rootScope.host+'/login.php',login_params)
		.success(function(data, status, headers, config)
		{
		
		/*
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				title: 'error signing in using facebook please try again',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
				});				   
			}
			else 
			{		
		*/
				$localStorage.userid = data.response.userid;
				$localStorage.username = data.response.username;
				$localStorage.email = data.response.email;
				$localStorage.gender = data.response.gender;
				$localStorage.interested = data.response.interested;
				$localStorage.visiblity = data.response.visiblity;				
				$localStorage.age = data.response.age;
				$localStorage.minAge = data.response.minAge;
				$localStorage.MaxAge = data.response.maxAge;	
				$localStorage.nearby = data.response.nearby
				$localStorage.distance = data.response.distance				
				//alert (data.response.minAge)
				//alert (data.response.maxAge)
				$localStorage.image = data.response.image;
				
				setTimeout(function()
				{ 
					$rootScope.$broadcast('Matches_Controller');
					$rootScope.$broadcast('Likes_Controller');
				}, 100);
					
				$state.go('app.matches');
				$ionicSideMenuDelegate.canDragContent(true);
				//console.log($localStorage)
		//	}
		})
		.error(function(data, status, headers, config)
		{

		});

				
	}

	

	
	$scope.LoadImageModal = function()
	{
			$ionicModal.fromTemplateUrl('templates/image_picker_modal.html', {
			scope: $scope
		  }).then(function(ImageModalShow) {
			$scope.ImageModalShow = ImageModalShow;
			$scope.ImageModalShow.show();
		  });
	}
	$scope.closePickerModal = function()
	{
		$scope.ImageModalShow.hide();
	}
	$scope.CameraOption = function() 
	{
		
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'select an option:',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'CAMERA',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'PHOTO GALLERY',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   	   {
		text: 'CANCEL',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });

   
	};

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) {
		 var options ;
		 
		
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				  
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
				//correctOrientation : true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{

			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			
				$timeout(function() {
   
   
			//$scope = $rootScope.register ;
			$scope.image.defaultimage = 'img/register/22.png';
			
			
			//data = JSON.stringify(data);
			//alert (data);
			//alert(data.response)
			if (data.response) {
				$scope.responseimage = data.response;
				$scope.image.submit = false;
				$scope.image.defaultimage = $rootScope.host+data.response;
				$scope.profilePicture = $rootScope.host+data.response;
				
			}
				
			   }, 1000);

		}
		
		$scope.onUploadFail = function(data)
		{
			$scope.image.submit = false;

			//alert("onUploadFail : " + data)
		}
		
    }
	
	
	$scope.register = function()
	{		
			$rootScope.currentPage = "register"
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			var emailRegex = /\S+@\S+\.\S+/;
			
			$scope.AgeRange = 
			{
				"minAge" : $scope.registerFields.minAge,
				"maxAge" : $scope.registerFields.maxAge
			};

			register_params = {
					"name" : $scope.registerFields.name,
					"username" : $scope.registerFields.username,
					"email" : $scope.registerFields.email,
					"password" : $scope.registerFields.password,
					"gender" : $scope.registerFields.gender,
					"interested" : $scope.registerFields.interested,
					"age" : $scope.registerFields.age,
					"minAge" : $scope.AgeRange.minAge,
					"maxAge" : $scope.AgeRange.maxAge,					
					"image" : $scope.responseimage,
//					"facebookid" : $scope.registerFields.facebookid 
					}
					
			if ($scope.registerFields.name =="")
			{
				$ionicPopup.alert({
				title: 'please fill in your full name',
				buttons: [{
				text: 'OK',
				type: 'button-positive',
				  }]
			   });					
			}
		/*
			else if ($scope.registerFields.username =="")
			{
				$ionicPopup.alert({
				title: 'please fill in a username',
				buttons: [{
				text: 'OK',
				type: 'button-positive',
				  }]
			   });					
			}
		*/
			else if ($scope.registerFields.email =="")
			{
				$ionicPopup.alert({
				title: 'please fill in your email',
				buttons: [{
				text: 'OK',
				type: 'button-positive',
				  }]
			   });					
			}
			
			else if (emailRegex.test($scope.registerFields.email) == false)
			{

				$ionicPopup.alert({
				title: 'email address not valid please correct',
				buttons: [{
				text: 'OK',
				type: 'button-positive',
				  }]
			   });	

			   $scope.registerFields.email =  '';
			}

		
			else if ($scope.registerFields.password =="")
			{
				$ionicPopup.alert({
				title: 'please fill in a password',
				buttons: [{
				text: 'OK',
				type: 'button-positive',
				  }]
			   });	
			}
			
			else if (!$scope.responseimage)
			{
				$ionicPopup.alert({
				title: 'please pick a profile picture',
				buttons: [{
				text: 'OK',
				type: 'button-positive',
				  }]
			   });					
			}
			
			else
			{
				//console.log(register_params)
				$http.post($rootScope.host+'/register.php',register_params)
				.success(function(data, status, headers, config)
				{
					
					if (data.response.status == 0)
					{
						$ionicPopup.alert({
						title: 'email already taken please choose a different one',
						buttons: [{
						text: 'OK',
						type: 'button-positive',
						  }]
					   });		

						$scope.registerFields.email = '';					   
					}
					else
					{
						$localStorage.name = $scope.registerFields.name;
						$localStorage.userid = data.response.userid;
						$localStorage.username = $scope.registerFields.username;
						$localStorage.email = $scope.registerFields.email;
						$localStorage.gender = $scope.registerFields.gender;
						$localStorage.interested = $scope.registerFields.interested;
						$localStorage.age = $scope.registerFields.age;
						$localStorage.minAge = 18;
						$localStorage.MaxAge = 55;
						$localStorage.image = $scope.responseimage;
						$localStorage.visiblity = 0;
						$scope.registerFields.name = '';
						$scope.registerFields.username = '';
						$scope.registerFields.email = '';
						$scope.registerFields.password = '';	
						$scope.registerFields.facebookid  = '';
						
						$localStorage.nearby = 1;
						$localStorage.distance = 10;

						
						setTimeout(function()
						{ 
							$rootScope.$broadcast('Matches_Controller');
							$rootScope.$broadcast('Likes_Controller');
						}, 100);
						
						$state.go('app.matches');						
					}
						

				})
				.error(function(data, status, headers, config)
				{

				});
			}
	}
})

.controller('SettingsCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicActionSheet,$ionicSideMenuDelegate) 
{
	$rootScope.currentPage = "settings"
	$ionicSideMenuDelegate.canDragContent(false);
	//alert ($localStorage.visiblity);
	
	if ($localStorage.visiblity == 1)
		$scope.visiblity = true;
	else
		$scope.visiblity = false;
	
	if ($localStorage.nearby == 1)
		$scope.nearby = true;
	else
		$scope.nearby = false;	

	
	$scope.fields = 
	{
		"interested" : "",
		"nearBy" : $scope.nearby,
		"visiblity" : $scope.visiblity,
		"minAge": $localStorage.minAge,
		"maxAge" : $localStorage.MaxAge,
		"minDistance" : $localStorage.distance,//10,
		"maxDistance" : 50
	}
	

	
	$scope.fields.interested = $localStorage.interested;
	
	$scope.LogOutBtn = function()
	{
			var hideSheet = $ionicActionSheet.show({		
			destructiveText: 'Logout',
			titleText: 'Are you sure you want to logout?',
			cssClass: 'social-actionsheet',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			buttonClicked: function(index) {
				//Called when one of the non-destructive buttons is clicked,
				//with the index of the button that was clicked and the button object.
				//Return true to close the action sheet, or false to keep it opened.
				return true;
			},
			destructiveButtonClicked: function()
			{
				//Called when the destructive button is clicked.
				//Return true to close the action sheet, or false to keep it opened.
					$localStorage.name = '';
					$localStorage.userid = '';
					$localStorage.username = '';
					$localStorage.email = '';
					$localStorage.gender = '';
					$localStorage.interested = '';
					$localStorage.age = '';
					$localStorage.minAge = '';
					$localStorage.MaxAge = '';	
					$localStorage.visiblity	= '';
					$localStorage.nearby = '';
					$localStorage.distance = '';
					$localStorage.image = '';
					$scope.isEnter = 0;
					$ionicSideMenuDelegate.toggleLeft();
					$state.go('app.main');
					$ionicSideMenuDelegate.canDragContent(false);
			}
		});	
	}
	


	$scope.EditProfile = function()
	{
		$ionicSideMenuDelegate.toggleLeft();
		window.location.href = "#/app/profile";
	}
	

	
	$scope.onChangeSave = function()
	{
		$scope.saveChangesBtn();
	}
	
	$scope.saveChangesBtn = function()
	{
		
		
		//alert ($scope.fields.nearBy);
		//alert ($scope.fields.minDistance);
		//return;
	
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		
		if ($scope.fields.visiblity == true)
			$scope.invisiblity  = 1;
		else
			$scope.invisiblity  = 0;

		if ($scope.fields.nearBy == true)
			$scope.isNearBy  = 1;
		else
			$scope.isNearBy  = 0;


		
		update_params = 
		{
			"user" : $localStorage.userid,
			"interested" : $scope.fields.interested,
			"invisiblity" : $scope.invisiblity,			
			"minAge" : $scope.fields.minAge,
			"maxAge" : $scope.fields.maxAge,
			"nearBy" : $scope.isNearBy,
			"distance" : $scope.fields.minDistance
		}
		

	//console.log(update_params)
	$http.post($rootScope.host+'/update_settings.php',update_params)
	.success(function(data, status, headers, config)
	{
		
		//alert ("saved successfully");
		$localStorage.interested = $scope.fields.interested;
		$localStorage.minAge = $scope.fields.minAge;
		$localStorage.MaxAge = $scope.fields.maxAge;

		$localStorage.nearby = $scope.isNearBy;
		$localStorage.distance = $scope.fields.minDistance;
		$rootScope.$broadcast('Matches_Controller');				
	})
	.error(function(data, status, headers, config)
	{

	});
				
				
	}

	$scope.refreshCtrl = function()
	{	
		//$rootScope.$broadcast('Matches_Controller');
	
		$rootScope.$broadcast('refreshMatches');
		$state.go("app.matches");
		//window.location.href = "#/app/matches";

	}
	
	$scope.deleteChatBtn = function()
	{
		$scope.deleteMatchesBtn();
		confirmbox = confirm("are you sure u want to delete?");
		if (confirmbox)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
			delete_params = {}

			//console.log(login_params)
			$http.post($rootScope.host+'/delete_chat.php',delete_params)
			.success(function(data, status, headers, config)
			{
				
			})
			.error(function(data, status, headers, config)
			{

			});
		}
	}


	$scope.deleteMatchesBtn = function()
	{
		confirmbox = confirm("are you sure u want to delete?");
		if (confirmbox)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			delete_params = {
						
							}
				//console.log(login_params)
				$http.post($rootScope.host+'/delete_matches.php',delete_params)
				.success(function(data, status, headers, config)
				{
					
				})
				.error(function(data, status, headers, config)
				{
	
				});
		}

	}

	
	//console.log($localStorage)
})


.controller('ProfileCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$cordovaCamera,$ionicPopup,$timeout,$ionicSideMenuDelegate) 
{
	$rootScope.currentPage = "profile"
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>'
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.showsubmit = false;
	$scope.defaultimage = $localStorage.image;
	
	$scope.fields =
	{
		"email" : $localStorage.email,
		"password" : ""
	}

	/*
		$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		$scope.showsubmit = true;		
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			if (data.response.imagepath) {
				$scope.image = data.response.imagepath;
				$scope.showsubmit = false;
				$scope.defaultimage = $rootScope.host+data.response.imagepath;
			}
         	//alert("p: "+data)  
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};
	*/
		$scope.takePicture = function(index) {
		 var options ;
		 $scope.showsubmit = true;
		 
		
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation : true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{

			$scope.imgURI = imageData
			
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			
				$timeout(function() {
   
   
			//$scope = $rootScope.register ;
			
			
			//data = JSON.stringify(data);
			//alert (data);
			//alert(data.response)
			//alert (data.response)
			if (data.response) {
				$scope.showsubmit = false;
				$scope.image = data.response;
				$scope.defaultimage = $rootScope.host+data.response;
				//$scope.responseimage = data.response;
				//$scope.image.submit = false;
				//$scope.image.defaultimage = $rootScope.host+data.response;
				//$scope.profilePicture = $rootScope.host+data.response;
				
			}
				
			   }, 1000);

		}
		
		$scope.onUploadFail = function(data)
		{
			$scope.showsubmit = false;

			//alert("onUploadFail : " + data)
		}
		
    }
	
	$scope.profileUpdate = function()
	{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		if ($scope.image)
		{
			$scope.profileimage = $scope.image;
		}			
		else {
			$scope.profileimage = $localStorage.image;
		}
			
				update_params = 
					{
					"user" : $localStorage.userid,
					"email" : $scope.fields.email,
					"password" : $scope.fields.password,			
					"image" : $scope.profileimage
					}


				//console.log(update_params)
				$http.post($rootScope.host+'/update_user.php',update_params)
				.success(function(data, status, headers, config)
				{
						$ionicPopup.alert({
						title: 'saved successfully',
						buttons: [{
						text: 'OK',
						type: 'button-positive',
						  }]
					   });	
					   
					//$localStorage.email = $scope.fields.email;
					$localStorage.image = $scope.profileimage;
				})
				.error(function(data, status, headers, config)
				{

				});		
		
			$state.go('app.matches');
	}
	
})

.controller('MatchesCtrl', function($scope,$http,$rootScope,$ionicModal,$stateParams,$localStorage,$cordovaGeolocation,$ionicLoading,$state,$ionicSideMenuDelegate,$ionicPlatform,GeoAlert,$ionicPopup,MatchesFactory,$ionicViewSwitcher,$timeout) 
{
	$scope.$on('$ionicView.enter', function(e) {
		//$scope.matches = $rootScope.MatchesArray;
		$rootScope.currentPage = "matches";
		
		$scope.timeOutValue =
		{
			"value" : "0"
		}
		
		$scope.matchesfields = 
		{
			"location_lat" : "",//"32.7889446",
			"location_lng" : "",//"35.029007"
		}

	
		//kiryat ata
		//$scope.MainLat = '32.810045'
		//$scope.MainLong = '35.101850'
		
		//Tirat hacarmel
		//$scope.MainLat = '32.762103'
		//$scope.MainLong = '34.973277'
		
		
		//Hadera
		//$scope.MainLat = '32.434405'
		//$scope.MainLong = '34.922148'
		
		//Here	
		//$scope.MainLat = '32.811147'
		//$scope.MainLong = '35.070413'
		
		/*
		GeoAlert.begin($scope.MainLat ,$scope.MainLong , function() 
		{
      		GeoAlert.end();
      		/*navigator.notification.confirm
      		(
        		'You are near a target!',
        		onConfirm,
        		'Target!',
        		['Cancel','View']
      		);
      	});
		*/
   		
		$scope.updateTextInput = function()
		{
			console.log("timeOutValue : " + $scope.timeOutValue.value )
		}
    

			//$scope.$on('$ionicView.enter', function(e) {
			$ionicSideMenuDelegate.canDragContent(true);
			$scope.username = $localStorage.username;
			$scope.imagePath = $rootScope.host;
			$scope.halfColor = "green";
			$scope.userid = String($localStorage.userid);
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>'
			$scope.NewLikes = 0;
			$scope.NewMessages = 0;
			$scope.circleSize = window.innerWidth*0.8;
			$scope.pusher = $rootScope.pusher;
			
			var channel = $scope.pusher.subscribe('iStare');
			channel.bind($localStorage.userid, function(data) 
			{
				console.log("Push messages count")
				$scope.getMessageCount();
				
				 $timeout(function() {
					//alert (444);
				}, 300);		
			})
			
		
			
			var channel1 = $scope.pusher.subscribe('iStare_Likes');
			channel1.bind($localStorage.userid, function(data) 
			{
				$scope.NewLikes++;
				console.log("new like: " , data);
				console.log("new like match");
				console.log(data.wholikedyou);

			   $timeout(function() {
				 $scope.recivedLikeIcon(data.wholikedyou)
				 $scope.NewMessages++;
			   }, 300);
	   
				
				//$scope.sendLikeIcon(data.wholikedyou)
				
				//$scope.getMatches();
			})
		

			$scope.getMessageCount = function()
			{
				data_params = 
				{
					"user" : $localStorage.userid
				}
				
				$http.post($rootScope.host+'/get_message_count.php',data_params)
				.success(function(data, status, headers, config)
				{
					console.log("getMessageCount : " ,  data.response)
					//if (data.response)
					//$scope.NewMessages = data.response.length;
					

				   $timeout(function() {
					$scope.NewMessages = data[0].count;
				   }, 300);

	   
				})
				.error(function(data, status, headers, config)
				{

				});				
			}


			$scope.getMessageCount();
			
			
			
			$scope.checkLocationService = function()
			{
				
				if (window.cordova)
				{
					
					
					$ionicLoading.show({
					  template: 'Loading...'
					});
			
					
					CheckGPS.check(function(){
						
						//GPS is enabled!
						$ionicLoading.hide();
						var posOptions = {enableHighAccuracy: false};

						$cordovaGeolocation
							.getCurrentPosition(posOptions)
							.then(function (position) {
								$ionicLoading.hide();
								$scope.matchesfields.location_lat = position.coords.latitude
								$scope.matchesfields.location_lng = position.coords.longitude;
								$scope.getMatches();
							}, function (err) {
								$ionicLoading.hide();
								//alert(JSON.stringify(err));

								navigator.geolocation.getCurrentPosition(
									function(position) {
										alert (444);
										$scope.matchesfields.location_lat = position.coords.latitude
										$scope.matchesfields.location_lng = position.coords.longitude;
										$scope.getMatches();
										
									},
									function errorCallback(error) {
										alert ("123")
										$scope.getMatches();	
									},
									{
									  enableHighAccuracy: true,
									  timeout: 5000,
									  maximumAge: 0
									}
								);					
								//$scope.getMatches();			
							});		

					  },
					  function(){
						  
						$ionicLoading.hide();
						$scope.getMatches();
						//$rootScope.InitCheckGPS();
						/*
						var myPopup = $ionicPopup.show({
						title: "Please turn on Location Services (GPS)",
						scope: $rootScope,
						cssClass: 'custom-popup',
						buttons: [

					   {
						text: "Settings",
						type: 'button-positive',
						onTap: SwitchLocationSettings
					   },

					   ]
					  });
						  
					  */
					  });		
				}
				else
				{
					$scope.getMatches();
				}
			}
			
			
			function SwitchLocationSettings()
			{
				 if (ionic.Platform.isIOS() == true)
					 cordova.plugins.diagnostic.switchToSettings();
				 else
					 cordova.plugins.diagnostic.switchToLocationSettings();
			}
	
			document.addEventListener("resume", $scope.checkLocationService, false);

	
			$scope.getMatches = function()
			{
				
				
				matches_params = 
				{
					"user" : $localStorage.userid,
					"interested" : $localStorage.interested,
					"minAge" : $localStorage.minAge,
					"maxAge" : $localStorage.MaxAge,
					"nearby" : $localStorage.nearby,
					"distance" : $localStorage.distance,	
					"location_lat" : $scope.matchesfields.location_lat,
					"location_lng" : $scope.matchesfields.location_lng			
				}
		
				
				console.log("MMMM " , matches_params)
								
				MatchesFactory.loadMatches(matches_params).then(function(data)
				{
					
					if (data.response.length > 0)
					{
						$scope.matches = data.response;
						
						console.log("GetMatches : " , $scope.matches )
					}
				});				
			}
			
			$scope.checkLocationService();
			//$scope.getMatches();
			
			$scope.$watch($scope.matches, function(value) 
			 {
					console.log("MatchChange : " , $scope.matches)
     		 });
	  
	  
			$rootScope.$on("refreshMatches", function (event, args) {
			$scope.checkLocationService();	
			//$scope.getMatches();
			});

			
		
			$scope.getLocation = function()
			{
				GeoAlert.checkLocation(function() 
				{
					//$scope.checkLocationService();
					//$scope.getMatches();
				});
			}
			
		//	$scope.getLocation();	
		
		$scope.goMessages = function()
		{
			window.location.href = "#/app/likes";
			//$state.go('app.likes');
		}
		
		$scope.onSwipeLeft = function()
		{
			window.location.href = "#/app/likes";

			//$scope.goMessages();
		}

		$scope.onSwipeRight = function()
		{
			$ionicViewSwitcher.nextDirection("back");	
			window.location.href = "#/app/settings";

			//$scope.goSettings();
		}

		
		$scope.goSettings = function()
		{
			//$ionicConfigProvider.views.transition('platform')
			
			$ionicViewSwitcher.nextDirection("back");	
			
			$state.go("app.settings");

			//window.location.href = "#/app/settings";
			//$state.go('app.settings');
		}
		
		

				
		
		$scope.getDistance = function(Distance)
		{
			var d1 = Number(Distance);
			var dis = d1*1000;
			return dis;
		}
		$scope.Liked = function(id,index,userid)
		{
			if (($scope.matches[index].user_id == "" && $scope.matches[index].liked == 0) || ($scope.matches[index].user_id != $localStorage.userid && $scope.matches[index].liked == 1))
			{
			
				liked_params = 
				{
					"user" : $localStorage.userid,
					"recipent" : id
				}
			
				$http.post($rootScope.host+'/like.php',liked_params)
				.success(function(data, status, headers, config)
				{
					$scope.matches[index].liked = 1;
					
					if (data.response.status =="1")
					$scope.matches[index].bothliked = 1;
					//$scope.getMatches();			
				})
				.error(function(data, status, headers, config)
				{

				});
			}
		}


		$scope.checkLiked = function(bothliked,userid)
		{
			if (bothliked == 1)
			window.location.href = "#/app/chat/"+userid;
		}

		$scope.pusher = $rootScope.pusher;
		
		
		
		//when you recived like
		$scope.recivedLikeIcon = function(id)
		{
			console.log("Push : " + id)
			//$scope.sendLikeIcon(card.id);
			console.log($scope.matches[0] + " : "  + $scope.matches.length)
			for (i=0;i<$scope.matches.length;i++)
			{
				
				$scope.It = $scope.matches[i];
				console.log($scope.It.liked + " : " +$scope.It.recpient_ok+ " : " +$scope.It.sender_ok + " : "  + $scope.It + " : "  + id)
				if($scope.It.id == id)
				{
					//user_id_recipent checkDoubleMessages
					console.log("YESS  : " + $scope.matches[0].sender_ok + " : " + String($scope.matches[0].sender_ok))
					$scope.matches[i].liked = 1;
					//item.sender = String(id);
					
					if($scope.matches[i].sender_ok != 0)
					{
						console.log("s1")
						$scope.matches[i].recpient_ok = "1";
						console.log("s2")
						$scope.matches[i].recpient = String(id);
					}
					else
					{
						$scope.matches[i].sender_ok = 1;
						$scope.matches[i].sender = String(id);
						$scope.matches[i].recpient = 0
						console.log("SenderOk")
					}
				}
			}
		}
		
		//when you send like
		$scope.sendLikeIcon = function(id)
		{
			//console.log("Yes : ")
			//console.log($scope.matches)
			angular.forEach($scope.matches, function(item) 
			{
				if(parseInt(item.id) == parseInt(id))
				{
					console.log("YESS111")
					item.liked = "1";
					item.recpient = String(id);
					item.sender = String($localStorage.userid);
					
					if(item.sender_ok == 1)
					item.recpient_ok = "1";
					else
					item.sender_ok = "1";
					console.log(item)
					return;
				}
			})
		}
	
	$scope.transitionRight = function(card) 
	{
			
			liked_params = 
			{
				"sender" : $localStorage.userid,
				"recipent" : card.id,
				"time" : $scope.timeOutValue.value
			}
			
			$scope.phpUrl = '/like.php';
			
			if( $scope.timeOutValue.value > 0 )
			$scope.phpUrl = '/likeDelay.php';
			
			$scope.sendLikeIcon(card.id);
			$http.post($rootScope.host+''+$scope.phpUrl,liked_params)
			.success(function(data, status, headers, config)
			{
				setTimeout(function()
				{ 
					console.log("T3 : " , data);
					$rootScope.$broadcast('Likes_Controller');
				}, 100);	
				//$scope.getMatches();				
			})
			.error(function(data, status, headers, config)
			{

			});	
	};
	
		$scope.showModal = function()
		{
			$ionicModal.fromTemplateUrl('image-modal.html', {
				scope: $scope
			  }).then(function(imageModal) {
				$scope.imageModal = imageModal;
				$scope.imageModal.show();
			  }); 
		}
		$scope.closeModal = function()
		{
			$scope.imageModal.hide();
		}
		$ionicModal.fromTemplateUrl('templates/like_modal.html', {
			scope: $scope
		  }).then(function(modal) {
			$scope.modal = modal;
		  });

		 $scope.closeLikeModal = function(type) 
		 {
			 $rootScope.currentPage = "matches"
			 
			 if(type == 1)
			 $scope.transitionRight($scope.matches[$scope.currentUser])
			  	
			 $scope.modal.hide();
		 };

		 $scope.showLikeModal = function(num,sender_ok,recpient_ok,user) 
		 {
			 $rootScope.currentPage = "like"
			 console.log("ShowModsl : " + $scope.matches[num].sender  + " : " + $localStorage.userid + " : " + $scope.matches[num].sender_ok)
			// if(data.sender == $localStorage.userid && data.sender_ok == 1)
			if($scope.matches[num].sender != $localStorage.userid || $scope.matches[num].sender_ok != 1)
			{
				$scope.currentUser = num;
				$scope.modal.show();
			}
			else
			{
				if (sender_ok == 1 && recpient_ok == 1)
				{
					window.location.href = "#/app/chat/"+user;
				}
				
				else
				{
					if ($scope.matches[num].gender == 1)
					{
						$scope.ref = "his";
					}
					else
					{
						$scope.ref = "her";
					}
						$ionicPopup.alert({
						 title: 'waiting for '+$scope.ref+' like',
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });						
				}
			}
		 };
		 
		 $scope.ShowRightMenu = function()
		 {
			 $ionicSideMenuDelegate.toggleRight();
		 }

		$scope.refreshMatches = function()
		{
			$scope.checkLocationService();
			//$scope.getMatches();
			$scope.$broadcast('scroll.refreshComplete');
		}


			
		
		
	//  });	
		
	/*$scope.cards = [];
	$scope.addCard = function(img, name,id,user_id,liked) 
	{
		var newCard = {image: img, name: name,id:id,user_id:user_id,liked:liked};
			//newCard.id = Math.random();
			$scope.cards.unshift(angular.extend({}, newCard));
		};
		
		$scope.addCards = function(count) {

			matches_params = 
			{
				"user" : $localStorage.userid,
				"interested" : $localStorage.interested,
				"minAge" : $localStorage.minAge,
				"maxAge" : $localStorage.MaxAge
			}
			
			$http.post($rootScope.host+'/get_matches.php',matches_params).then(function(value) {
				angular.forEach(value.data.response, function (v) {
					//console.log (v);
					//alert (v.username);
					$scope.addCard($rootScope.host+v.image, v.username,v.id,v.user_id,v.liked);
				});
			});
		};
	
	
		$scope.addCards(5);
	
		$scope.cardDestroyed = function(index) {
			$scope.cards.splice(index, 1);
			$scope.addCards(1);
		};
	
		$scope.transitionOut = function(card) {
			console.log('card transition out');
		};
	
		
		$scope.transitionLeft = function(card) {
			console.log('card removed to the left');
			console.log(card);
		};/*

	*/	
	});

	$ionicViewSwitcher.nextDirection('forward');		
			
})

.controller('LikesCtrl', function($scope,$http,$rootScope,$ionicModal,$stateParams,$localStorage,$state,$ionicSideMenuDelegate,$ionicHistory) 
{
//	$scope.$on('Likes_Controller', function(event, args) 
//	{
		$scope.shouldShowDelete = false;
		$scope.shouldShowReorder = false;
		$scope.listCanSwipe = true
		$ionicSideMenuDelegate.canDragContent(false);
		$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>'
		$scope.pusher = $rootScope.pusher;
		$scope.imageSrc = '';
		$scope.ActiveTab = 0;
		
		var channel = $scope.pusher.subscribe('iStare_Likes');
		
		channel.bind($localStorage.userid, function(data) 
		{	
			//alert("Likes")
			$scope.getLikes();
			
		})
	
		$scope.imagePath = $rootScope.host;
		$scope.userid = $localStorage.userid;
		$scope.username = $localStorage.username;
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		
		var channel1 = $scope.pusher.subscribe('iStare');
		
		channel1.bind($localStorage.userid, function(data)
		{
			$scope.getLikes();
		})



		$scope.getLikes = function()
		{
			likes_params = 
			{
				"user" : $localStorage.userid
			}
			
			$http.post($rootScope.host+'/get_messages.php',likes_params)
			.success(function(data, status, headers, config)
			{	
				$scope.LikesArray = data.likes.response;
				$scope.messagesArray = data.messages.response; 
				$rootScope.MainArray = data;
				console.log ('messages array: ', data)
				console.log ('messages : ', $scope.messagesArray)
			})
			.error(function(data, status, headers, config)
			{
	
			});				
		}
	
		$scope.getLikes();
		
		$scope.LikedBack = function(id,index,senderid,recipentid)
		{
			if(recipentid == $localStorage.userid)
			{
				$scope.currentUser = index;
				$scope.showLikeModal(index)
				$scope.senderId = senderid;
			}
		}
		
		
		$scope.checkLiked = function(status,recipent,sender)
		{
			if ($localStorage.userid == recipent)
			{
				$scope.chat = sender;
			}
			else 
			{
				$scope.chat = recipent;
			}
		
			if (status == 1)
			{
				$ionicSideMenuDelegate.toggleRight();
				window.location.href = "#/app/chat/"+$scope.chat;
			}
		}
		
		$scope.onSwipeRight= function()
		{
			window.location.href = "#/app/matches";
		}
		
		$scope.goToChat = function(Id,Item)
		{
			console.log("Check")
			$ionicHistory.nextViewOptions({
				disableAnimate: true,
				expire: 300
			});
			
			if (Item == 0)
			{
				$ionicSideMenuDelegate.toggleRight();
				window.location.href = "#/app/chat/"+Id;				
			}
			else if (Item.recpient_ok == 1 && Item.sender_ok == 1)
			{
				$ionicSideMenuDelegate.toggleRight();
				window.location.href = "#/app/chat/"+Id;				
			}

		}
		

		//products modal.		
		$ionicModal.fromTemplateUrl('templates/like_page_modal.html', {
			scope: $scope
		  }).then(function(modal) {
			$scope.modal = modal;
		  });

		 $scope.closeLikeModal = function(type) 
		 {
				if(type == 1)
			   	$scope.sendLike($scope.senderId)
			  	
				$scope.modal.hide();
		 };

		 $scope.showLikeModal = function(num) 
		 {
			$scope.currentUser = num;
			$scope.modal.show();
		 };
		 
		 $scope.sendLike = function(senderId) 
		{
			console.log("card");
			console.log(senderId + " : "  + $localStorage.userid  ) ;
	
			liked_params = 
			{
				"sender" : $localStorage.userid,
				"recipent" : senderId
			}
			
			$http.post($rootScope.host+'/like.php',liked_params)
			.success(function(data, status, headers, config)
			{
				$scope.getLikes();				
			})
			.error(function(data, status, headers, config)
			{

			});	
		};
		
		
		
		$scope.getMessages = function()
		{
			data_params = 
			{
				"user" : $localStorage.userid
			}
			$http.post($rootScope.host+'/get_messages.php',data_params)
			.success(function(data, status, headers, config)
			{	
				//$scope.messagesArray = data.response;
				console.log("messagesArray12 : " )
				console.log(data)
			})
			.error(function(data, status, headers, config)
			{
	
			});
		}	
		


		$scope.searchMessages = function(value)
		{
			//console.log("Filter : " + value.username_id + " : " + $localStorage.userid)
			//if (value.username_id != $localStorage.userid)
			//{
				return true;
			//}
		}
		
		$scope.checkDoubleMessages = function(data)
		{
				//console.log("Messages")
				//console.log($rootScope.MainArray.messages.response)
				//console.log(data)
				if(data.sender_ok == 1 && data.recpient_ok == 1 && $rootScope.MainArray.messages.response)
				{	
					for(var i=0;i< $rootScope.MainArray.messages.response.length;i++)
					{
						//console.log("Filter In : " + $rootScope.MainArray.messages.response[i].username_id + " : " + data.user_id_recipent)
						if($rootScope.MainArray.messages.response[i].username_id == data.user_id_recipent || $rootScope.MainArray.messages.response[i].username_id == data.user_id_sender )
						{
							return false;
						}
					}
					
					
				}
				
				return true;
		}
		
		$scope.ShowImageModal = function(value,num)
		{
			//alert (value);
			
			if (num == 0)
			{
				if (value.user_id_sender == $localStorage.userid)
				{
					$scope.imageSrc =  value.image_recipent;
				}
				else 
				{
					$scope.imageSrc =  value.image_sender;
				}				
			}
			
			if (num == 1)
			{
				$scope.imageSrc = value;
			}
			
			
			if($scope.imageSrc.charAt(0) == "u")
			{
				$scope.imageSrc = $rootScope.host + "/"+$scope.imageSrc;
			}
			else
			{
				$scope.imageSrc = $scope.imageSrc;
			}
				
				
			
			//alert ($scope.imageSrc)
			
		   $ionicModal.fromTemplateUrl('image-modal2.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(modal) {
			  $scope.modal = modal;
			   $scope.modal.show();
			});
		}
		$scope.closeModal = function()
		{
			 $scope.modal.hide();
		}
		
		$scope.setActiveTab = function(tab)
		{
			$scope.ActiveTab = tab;
		}
		
		$scope.deleteMsg = function(index,item)
		{
			//console.log($localStorage.userid);
			//console.log(item);
			//return;
			

			$scope.SenderItem  =  item.sender_id;
			$scope.RecipentItem  =  item.recipent_id;


			data_params = 
			{
				"user" : $scope.RecipentItem, //$localStorage.userid,
				"sender" : $scope.SenderItem,
				"send" : 1
			}
			console.log(data_params)
			//return;
			
			$http.post($rootScope.host+'/delete_messages.php',data_params)
			.success(function(data, status, headers, config)
			{	
				//alert (data);
				$scope.messagesArray.splice(index, 1);
			})
			.error(function(data, status, headers, config)
			{

			});			
			

		}
		
		$scope.deleteLike = function(index,item)
		{
			console.log(item);
			$scope.recipentItem2 = item.user_id_recipent;
			$scope.SenderItem2  =  item.user_id_sender;

			data_params = 
			{
				"user" : $scope.recipentItem2,
				"sender" : $scope.SenderItem2,
				"send" : 1
			}
			$http.post($rootScope.host+'/delete_likes.php',data_params)
			.success(function(data, status, headers, config)
			{	
				$scope.LikesArray.splice(index, 1);
			})
			.error(function(data, status, headers, config)
			{

			});	
		}
		
//	});		
})	
	

.controller('MessagesCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state) 
{
	// $scope.$on('$ionicView.enter', function(e) {
 
		$scope.imagePath = $rootScope.host;
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$scope.username = $localStorage.username;
		$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>'

		$scope.pusher = $rootScope.pusher;
		var channel = $scope.pusher.subscribe('iStare');
		channel.bind($localStorage.userid, function(data)
		{
	
		  $scope.getMessages();
		  console.log("push");
	
		})


	
		$scope.getMessages = function()
		{
			data_params = 
		{
			"user" : $localStorage.userid
		}
		$http.post($rootScope.host+'/get_messages.php',data_params)
		.success(function(data, status, headers, config)
		{	
			$scope.messagesArray = data.response;
			console.log("messagesArray")
			console.log($scope.messagesArray)
		})
		.error(function(data, status, headers, config)
		{

		});
		}	
		
		$scope.getMessages();


		$scope.searchMessages = function(value)
		{
			
			if (value.username_id != $localStorage.userid)
			{
				return true;
			}
		}

	//});	
})


.controller('ChatCtrl', function($scope,$http,$rootScope,$stateParams,$localStorage,$state,$ionicSideMenuDelegate,$ionicScrollDelegate,$timeout,$ionicLoading) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //{
	
	  $scope.myinput = "";
	  
	$scope.test = function() {
        document.getElementById('thebox').focus();
        
		//angular.element(document.getElementById("thebox")).css("background-color","red");
		angular.element(document.getElementById("thebox")).focus();
		$scope.myinput.focus();
    };
	  
	$rootScope.currentPage = "chat"
	$ionicSideMenuDelegate.canDragContent(true);
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	$scope.chatArray = new Array();
	$scope.imagePath = $rootScope.host;
	$scope.user_local_storage = $localStorage.userid;
	$scope.navTitle='<img class="title-image" src="img/logo.png" style="width:70px; margin-top:5px;"/>'
	$scope.UserData = "sss";
	
	$scope.chatFields = 
	{
		"chatbox" : ""	
	};
	
	$scope.getUser = function(UserId)
	{
		data_params = 
		{
			"user" :UserId
		}
		
		$http.post($rootScope.host+'/getUser.php',data_params)
		.success(function(data, status, headers, config)
		{	
			$scope.UserData = data
		})
		.error(function(data, status, headers, config)
		{

		});	
	}
	
	
	$scope.getUser($stateParams.id)
	
	$scope.pusher = $rootScope.pusher;
    var channel = $scope.pusher.subscribe('iStare');
    channel.bind($localStorage.userid, function(data)
	{
	   
	  if ($rootScope.State =="app.chat")
		$scope.getChatHistory();	
	  console.log("push");

	})
	
	
	//get chat messages
	$scope.getChatHistory = function() 
	{
		$ionicLoading.show({
          template: 'Loading...'
        });
		
		data_params = 
		{
			"user" : $localStorage.userid,
			"recipent" : $stateParams.id
		}
		//console.log(data_params)
		$http.post($rootScope.host+'/get_chat_history.php',data_params)
		.success(function(data, status, headers, config)
		{	
			$ionicLoading.hide();
			if (data.response)
			{
				$scope.chatArray = data.response;
				//$scope.chatArray.reverse();
				console.log("Ca : " + $localStorage.userid)
				console.log(data.response)
			}
			
			$ionicScrollDelegate.scrollBottom();
		})
		.error(function(data, status, headers, config)
		{

		});	
	}

	$scope.getChatHistory();	
	
	$scope.chatBlur = function(e)
	{
		//alert(e.target.id)
		//_element[0].focus();
		//e.target.css("background-color","green");
		//e.target.focus();
		//angular.element(document.getElementById("chatInput")).css("background-color","red");
		//angular.element(document.getElementById("chatInput")).focus();
	}
	
	$scope.sendChat = function()
	{
		//window.cordova.plugins.Keyboard.show();
		
		
		if ($scope.chatFields.chatbox)
		{
			//console.log($scope.chatArray);
			chat_params = 
				  {
					"user" : $localStorage.userid,
					"recipent" : $stateParams.id,
					"message" : $scope.chatFields.chatbox,
					"username" :  $localStorage.username,
					"image" : $localStorage.image
					}
				//console.log(chat_params)
				$http.post($rootScope.host+'/send_chat_message.php',chat_params)
				.success(function(data, status, headers, config)
				{
					$scope.date = new Date()
					$scope.hours = $scope.date.getHours()
					$scope.minutes = $scope.date.getMinutes()
				
					if ($scope.hours < 10)
					$scope.hours = " " + $scope.hours
					
					if ($scope.minutes < 10)
					$scope.minutes = "0" + $scope.minutes
				
					$scope.time = $scope.hours+':'+$scope.minutes;
					
					$scope.chat  = {
						"username" : $localStorage.username,
						"text" : $scope.chatFields.chatbox,
						"image" : $localStorage.image,
						"userid" : $localStorage.userid,
						"time" : $scope.time
					}
	
					console.log("chatArray")
					console.log($scope.chatArray)
					$scope.chatArray.push($scope.chat);
					$scope.chatFields.chatbox = '';
					//angular.element(document.getElementById("chatInput")).css("background-color","red");
					angular.element(document.getElementById("chatInput")).focus();
					$scope.chatFields.chatbox.focus();
					//$ionicScrollDelegate.scrollBottom();
				})
				.error(function(data, status, headers, config)
				{

				});			
		}
	}
	
		$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendChat();
		}
	}
	
	
  //});	
})

.filter('cutString_ListPage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' …');
    };
})



.filter('checkLetter', function () {
    return function (value) 
	{
		if (value == 1)
		{
			return true;
		}
		else 
		{
			return false;
		}
    };
})



.filter('checkWho', function ($localStorage) {
    return function (value) 
	{	
	 if (value.user_id_recipent == $localStorage.userid)
	{

		if (value.gender_sender == "1")
		{
			return "he";
		}
		else
		{
			return "she";
		}
		
	}
	
	else if (value.user_id_sender == $localStorage.userid)
	{
		if (value.gender_recipent == "1")
		{
			return "him";
		}
		else
		{
			return "you";
		}
	}
	
    };
})


.filter('checkUsername', function ($localStorage) {
    return function (value) 
	{
		if (value.user_id_sender == $localStorage.userid)
		{
			return value.username_recipent;
		}
		else {
			return value.username_sender;
		}
		
    };
})

.filter('checkUserId', function ($localStorage) {
    return function (value) 
	{
		if (value.user_id_sender == $localStorage.userid)
		{
			return value.user_id_recipent;
		}
		else {
			if(value.user_id_sender != "")
			return value.user_id_sender;
			else
			return 0;
		}
		
    };
})



.filter('showImage', function ($localStorage) {
    return function (value) 
	{
		if (value.user_id_sender == $localStorage.userid)
		{
			return value.image_recipent;
		}
		else {
			return value.image_sender;
		}
		
    };
})



.filter('getLikedImage', function($localStorage) 
{
	return function(data) 
	{
		//console.log("getLikedImage")
		//console.log(data.sender + " : " + $localStorage.userid + " : " + data.sender_ok + " : " + data.recpient_ok)
		var imgUrl = "";
		
		if(data.sender == $localStorage.userid && data.sender_ok == 1)
		{
			imgUrl = "img/icons/heart2.png"
		}
		else if(data.sender != $localStorage.userid && data.sender_ok == 1)
		{
			imgUrl = "img/icons/heart1.png"
		}
		
		if(data.recpient_ok  == 1 && data.sender_ok == 1)
		{
			imgUrl = "img/icons/grey_mail.png"
		}
		return imgUrl;
	};
})

.filter('getLikedImagePage', function($localStorage) 
{
	return function(data) 
	{
		var imgUrl = "";
		
		if(data.user_id_recipent == $localStorage.userid)
		{
			imgUrl = "img/icons/heart1.png"
		}
		else if(data.user_id_sender == $localStorage.userid)
		{
			imgUrl = "img/icons/heart2.png"
		}
		
		if(data.recpient_ok  == 1 && data.sender_ok == 1)
		{
			imgUrl = "img/icons/grey_mail.png"
		}
		
		return imgUrl;
	};
})


.filter('checkDoubleMessages', function($localStorage,$rootScope)
{
	return function(data) 
	{
		
	};
})


.filter('checkImgUrl', function ($rootScope) {
    return function (value) 
	{
		if(value)
		{
			var Img = "";
				
				if(value.charAt(0) == "u")
				{
					Img = $rootScope.host + "/"+value;
				}
				else
				{
					Img = value
				}
				return Img;
		}
    };
})

.filter('lastseenDate', function ($rootScope) {
    return function (value) 
	{
		if(value != "s")
		{
			var splitLastSeen = '';
		
			if (value.lastLogin)
			splitLastSeen =  value.lastLogin.split(" ");
			else if (value.date)
			splitLastSeen =  value.date.split(" ");

			var date = splitLastSeen[0];
			var hour = splitLastSeen[1];
		
			var splitdate =  date.split("-");
			var splithour =  hour.split(":");
		
			return ''+splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0]+' '+ splithour[0]+':'+splithour[1]+'';
		}
		
    };
})


.service('UserService', function() {
  // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
  var setUser = function(user_data) {
    window.localStorage.starter_facebook_user = JSON.stringify(user_data);
  };

  var getUser = function(){
    return JSON.parse(window.localStorage.starter_facebook_user || '{}');
  };

  return {
    getUser: getUser,
    setUser: setUser
  };
})

/*
.factory('GeoAlert', function() 
{
	 console.log("shay")
	 $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	 var dist = "";
	 var interval;
	 var duration = 6000;
	 var long, lat;
	 var processing = false;
	 var callback;
	 var minDistance = 10;
	  
	 function hb() 
	 {
		 console.log("shay4")
		  if(processing) 
		  {
			  alert("Gps Close")
			  return;
		  }
		  
		  processing = true;
		  navigator.geolocation.getCurrentPosition(function(position) 
		  {
			processing = false;
			console.log(position.coords.latitude, position.coords.longitude);
			var dist = String(position.coords.latitude) + ":" + String(position.coords.longitude)
			sendLocationToServer()
		  });
	  }
	  
	  function sendLocationToServer()
	  {
			update_params = 
			{
				"user" : $localStorage.userid,
				"location" : dist
			}

			$http.post($rootScope.host+'/update_location.php',update_params)
			.success(function(data, status, headers, config)
			{
				alert("Location Update")				
			})
			.error(function(data, status, headers, config)
			{

			});
	  }
   
	 return {
     begin:function(lt,lg,cb) 
	 {
		  long = lg;
       lat = lt;
       callback = cb;
		 console.log("shay1")
       	interval = window.setInterval(hb, duration);
		console.log("shay2")
       	hb();
		console.log("shay3")
     }, 
     end: function() {
       window.clearInterval(interval);
     },
     setTarget: function(lg,lt) {
       long = lg;
       lat = lt;
     }
   };
})
	*/
	

.factory('GeoAlert', function($localStorage,$rootScope,$http) {
   var interval;
   var duration = 6000;
   var long, lat;
   var processing = false;
   var callback;
   var FirstCallback;
   var minDistance = 10;
   var dist = "";
    
   function hb(Index) 
   { 
		  if(processing) 
		  {
			  //alert("Gps Close")
			  if(Index == 1)
			  {
				 dist = "";
			  	 sendLocationToServer(Index)
			  }
			  else
			  return ;
		  }
		  
		  processing = true;
		  navigator.geolocation.getCurrentPosition(function(position) 
		  {
			processing = false;
			console.log(position.coords.latitude, position.coords.longitude);
			dist = String(position.coords.latitude) + ":" + String(position.coords.longitude)
			sendLocationToServer(Index)
		  });
   }
   
    function sendLocationToServer(Index)
	{
			update_params = 
			{
				"user" : $localStorage.userid,
				"location" : dist
			}

			$http.post($rootScope.host+'/update_location.php',update_params)
			.success(function(data, status, headers, config)
			{
				if(Index == 1)
			 	 FirstCallback();
				else
				callback()				
			})
			.error(function(data, status, headers, config)
			{

			});
	  }
	  
  
   
   return {
     begin:function(lt,lg,cb) {
       long = lg;
       lat = lt;
       callback = cb;
       interval = window.setInterval(hb, duration);
       hb(0);
     }, 
     end: function() {
       window.clearInterval(interval);
     },
	 checkLocation: function(cb) {
		FirstCallback = cb;
		 hb(1);
     },
     setTarget: function(lg,lt) {
       long = lg;
       lat = lt;
     }
   };
   
})


/*.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {

      $timeout(function() {
        element[0].focus(); 
      });
    }
  };
})*/



.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      scope.$watch(attrs.focusMe, function(value) {
        if(value === true) { 
          console.log('value=',value);
          //$timeout(function() {
            element[0].focus();
            scope[attrs.focusMe] = false;
          //});
        }
      });
    }
  };
})

.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});
/*
.factory('GeoAlert', function() {
   console.log('GeoAlert service instantiated');
   var interval;
   var duration = 6000;
   var long, lat;
   var processing = false;
   var callback;
   var minDistance = 10;
    
   // Credit: http://stackoverflow.com/a/27943/52160   
   function getDistanceFromLatLonInKm(lat1,lon1,la,lon2)
   {
		var R = 6371; // Radius of the earth in km
		var dLat = deg2rad(la-lat1);  // deg2rad below
		var dLon = deg2rad(lon2-lon1); 
		var a = 
			  Math.sin(dLat/2) * Math.sin(dLat/2) +
			  Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(la)) * 
			  Math.sin(dLon/2) * Math.sin(dLon/2); 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c; // Distance in km
    	return d;
   }
  
   function deg2rad(deg) 
   {
    	return deg * (Math.PI/180)
   }
   
   function hb() 
   {
      if(processing) 
	  {
		  alert("Gps Close")
		  return;
	  }
	  
      processing = true;
      navigator.geolocation.getCurrentPosition(function(position) {
        processing = false;
        console.log(lat, long);
        console.log(position.coords.latitude, position.coords.longitude);
        var dist = getDistanceFromLatLonInKm(lat, long, position.coords.latitude, position.coords.longitude);
        alert("dist in km is "+dist);
        if(dist <= minDistance) callback();
      });
   }
   
   return {
     begin:function(lt,lg,cb) {
       long = lg;
       lat = lt;
       callback = cb;
       interval = window.setInterval(hb, duration);
       hb();
     }, 
     end: function() {
       window.clearInterval(interval);
     },
     setTarget: function(lg,lt) {
       long = lg;
       lat = lt;
     }
   };
   
})


*/
