angular.module('starter.factories', [])


.factory('MatchesFactory', function($http,$rootScope,$localStorage,$ionicLoading) {
	var data = [];

	return {
		loadMatches: function(params){
			
			
			$ionicLoading.show({
			  template: 'Loading...'
			});
		
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
		
			return $http.post($rootScope.host+'/get_matches.php',params).then(function(resp)
			{
				$ionicLoading.hide();
				data = resp.data;
				$rootScope.MatchesArray = data;
				return data;
			});
		},
		/*
		getUser: function(id){
			for(i=0;i<users.length;i++){
				if(users[i].id == id){
					return users[i];
				}
			}
			return null;
		}
		*/
	}
})
