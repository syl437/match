// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','openfb', 'starter.controllers','ngStorage','uiSlider','rzModule','ionic.contrib.ui.tinderCards','ngCordova','starter.factories'])
.run(function($rootScope,$ionicPlatform,$http,$localStorage,OpenFB,$ionicPopup,$cordovaGeolocation,GeoAlert,$ionicConfig,$state) {
	$rootScope.host  = 'http://tapper.co.il/istare/php/';
	$rootScope.pusher = new Pusher('23db6ed760aede96ea63', {
			  encrypted: false
	});
	$rootScope.MainArray = "";
	$rootScope.register = "";
	$rootScope.currentPage = "matches";
	$rootScope.MatchesArray = [];
	OpenFB.init('141248142909432');
  	$ionicConfig.navBar.alignTitle('center');
	$rootScope.LocationAlertShown = 0;
	$rootScope.currState = $state;
	$rootScope.State = '';
	
	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
	  $rootScope.State = newValue;
	}); 
   /*if (window.cordova) {
    cordova.plugins.diagnostic.isLocationEnabled(
                  function(e) {
                      if (e){
                      	alert("ss")
                        //successFunctionCall();
                      }   
                      else {
                        alert("Location Not Turned ON");
                        cordova.plugins.diagnostic.switchToLocationSettings();
                      }
                  },
                  function(e) {
                      alert('Error ' + e);
                  }
              );
          }*/
		
  $ionicPlatform.registerBackButtonAction(function (event) {
	 if( $rootScope.currentPage = "matches")


		var confirmPopup = $ionicPopup.confirm({
			 title: 'would you like to close the app?',
		   });
		   confirmPopup.then(function(res) {
			 if(res) {
			   navigator.app.exitApp();
			 } else {
			   event.preventDefault();
			 }
		   });	

		   
		 
    /*if($ionicHistory.currentStateName() == "myiew"){
      ionic.Platform.exitApp();
      // or do nothing
    }
    else {
      $ionicHistory.goBack();
    }*/
  }, 100);
  
  
  $ionicPlatform.ready(function() {
	  
	  
	$rootScope.InitCheckGPS = function()
	{
		if (window.cordova){
			
		CheckGPS.check(function(){
			//GPS is enabled!

		  },
		  function(){
			
			//if ($rootScope.LocationAlertShown == 0)
			//{
				
				deregisterBackButton = $ionicPlatform.registerBackButtonAction(function(e){}, 401);
				
				
				var myPopup = $ionicPopup.show({
				title: "Please turn on Location Services (GPS)",
				scope: $rootScope,
				cssClass: 'custom-popup',
				buttons: [

			   {
				text: "Settings",
				type: 'button-positive',
				onTap: SwitchLocationSettings
			   },
			   /*
			   {
				text: "Cancel",
				type: 'button-assertive',
				onTap: function(e) {  
				  //alert (1)
				}
			   },
			   */
			   ]
			  });				
			//}

			  
			  //$rootScope.LocationAlertShown = 1;

		  });		
		}
	}

	
	$rootScope.InitCheckGPS();

	
	function SwitchLocationSettings()
	{
		 if (ionic.Platform.isIOS() == true)
			 cordova.plugins.diagnostic.switchToSettings();
		 else
			 cordova.plugins.diagnostic.switchToLocationSettings();
	}

	
	document.addEventListener("resume", $rootScope.InitCheckGPS, false);
	
	
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) 
	{
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
  });
})


  
.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
$ionicConfigProvider.backButton.previousTitleText(false).text('');

  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


  
   .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainController'
      }
    }
  })
  
 

   .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  
   .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'
      }
    }
  })
  

   .state('app.matches', {
    url: '/matches',
    views: {
      'menuContent': {
        templateUrl: 'templates/matches.html',
        controller: 'MatchesCtrl'
      }
    }
  })
  


   .state('app.likes', {
    url: '/likes',
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller: 'LikesCtrl'
      }
    }
  })

  
   .state('app.chat', {
    url: '/chat/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/chat.html',
        controller: 'ChatCtrl'
      }
    }
  })  
 /*
 .state('app.messages', {
    url: '/messages',
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller: 'MessagesCtrl'
      }
    }
  })  
  */
   .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })
 
   .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  }) 
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
